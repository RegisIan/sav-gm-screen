# Patch Notes:

## Version 1.0.3
Fixed Journal Entry link connecting to /packs.

## Version 1.0.2
Update module.json.

## Version 1.0.1
Updated module.json with manifest and download links.

## Version 1.0.0
Initial release.
